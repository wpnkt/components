import React from 'react';

const Control = ({
    value,
    placeholder,
    style,
    className,
    ...props
}) => {
    return (
        <input
            type="text"
            placeholder={value}
            value={value}
            readOnly
            className={['control','control--text',className].filter(a=>a).join(' ')}
            style={style}
            {...props}
        />
    );
};

export default Control;
