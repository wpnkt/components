"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _bootstrapMin = _interopRequireDefault(require("bootstrap/dist/css/bootstrap.min.css"));

var _shardsMin = _interopRequireDefault(require("shards-ui/dist/css/shards.min.css"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = {
  bootstrap: _bootstrapMin.default,
  shards: _shardsMin.default
};
exports.default = _default;