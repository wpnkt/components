import React from 'react';
import Tag from '../Tag';
import generateClasses from '../generateClasses';
import propsClass from './props';

const Text = ({
    color,
    size,
    transform,
    mod,
    children,
    style,
    className,
    as = 'span',
    ...props
}) => {

    const modClasses = generateClasses({
        base: 'text',
        mods: {
            color:color,
            size:size,
            transform:transform,
            mod:mod,
        },
        map: propsClass,
    });
    const classes = [
        'text',
        modClasses.join(' '),
        className,
    ];
    return  (
        <Tag
            as={as}
            style={style}
            className={classes.filter(c=>c).join(' ')}
            {...props}
        >
            {children}
        </Tag>
    );
};

export default Text;
