import React from 'react';

const Option = ({children}) => { return (<div>{children}</div>); };
const Popup = ({children}) => { return (<div>{children}</div>); };
const List = ({children}) => { return (<div>{children}</div>); };

const Value = ({children}) => { return (
    <div className="control_out">{children}</div>
); };

const Control = ({
    value,
    onChange,
    placeholder,
    style,
    className,
    options,
    multiple = false,
    ...props
}) => {

    return (
        <div className="control control--select">
            <Value>{value}</Value>
            <div className="control_in">
                <Popup>
                    <List>
                    { !value ? (
                        <Option >Wybierz</Option>
                    ) : null }
                    { options.map((opt,key)=>{
                        return (
                            <Option key={key} value={opt.value}>{opt.label}</Option>
                        );
                    }) }
                    </List>
                </Popup>
            </div>
        </div>
        <select
            type="text"
            placeholder={placeholder}
            value={value}
            onChange={(e)=>onChange(e.target.selectedOptions ? e.target.selectedOptions[0].value : '')}
            className={['control','control--select',className].filter(a=>a).join(' ')}
            style={style}
            {...props}
        >
            { !value ? (
                <option value={''} /*disabled="disabled"*/ >Wybierz</option>
            ) : null }
            { options.map((opt,key)=>{
                return (
                    <option key={key} value={opt.value}>{opt.label}</option>
                );
            }) }
        </select>
    );
};

export default Control;
