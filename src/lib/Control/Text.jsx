import React from 'react';

const Control = ({
    value,
    onChange,
    placeholder,
    style,
    className,
    ...props
}) => {
    return (
        <input
            type="text"
            placeholder={placeholder}
            value={value}
            onChange={(e)=>onChange(e.target.value)}
            className={['control','control--text',className].filter(a=>a).join(' ')}
            style={style}
            {...props}
        />
    );
};

export default Control;
