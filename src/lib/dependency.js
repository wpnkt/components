import bootstrap from 'bootstrap/dist/css/bootstrap.min.css';
import shards from "shards-ui/dist/css/shards.min.css";

export default {
    bootstrap:bootstrap,
    shards:shards
};
