import React from 'react';
import {default as Text} from './Text';
import {default as Number} from './Number';
import {default as Textarea} from './Textarea';
import {default as Html} from './Html';
import {default as Select} from './Select';
import {default as Checkbox} from './Checkbox';
import {default as Switch} from './Switch';
import {default as Search} from './Search';
import {default as File} from './File';
import {default as Value} from './Value';

const Control = ({
    type = 'text',
    ...props
}) => {
    switch(type){
        case 'value': return <Value {...props} />;
        case 'text': return <Text {...props} />;
        case 'number': return <Number {...props} />;
        case 'textarea': return <Textarea {...props} />;
        case 'html': return <Html {...props} />;
        case 'select': return <Select {...props} />;
        case 'checkbox': return <Checkbox {...props} />;
        case 'switch': return <Switch {...props} />;
        case 'search': return <Search {...props} />;
        case 'file': return <File {...props} />;
        default: return <Text {...props} />;
    }
};

export default Control;
