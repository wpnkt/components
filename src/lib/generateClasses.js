const generateClasses = ({
    base,
    mods,
    map,
}) => {
    const parts = Object.entries(mods).map(([key,value])=>{
        let valueParse = '';
        if(!value){
            return null;
        }
        if(typeof value === 'string'){
            valueParse = value.split(' ');
        }
        if(typeof value === 'number'){
            valueParse = [value];
        }
        if(Array.isArray(value)){
            valueParse = value;
        }
        return valueParse.map(str=>{
            return str && map[key] && map[key].includes(str) ? `${base}--${key}--${str}` : null;
        });
    }).filter(a=>a);
    return parts.map(part=>part.join(' '));
};

export default generateClasses;
