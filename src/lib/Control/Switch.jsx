import React from 'react';

const Control = ({
    checked,
    onChange,
    placeholder,
    style,
    className,
    children,
    ...props
}) => {
    return (
        <label>
            <input
                type="checkbox"
                placeholder={placeholder}
                checked={checked}
                onChange={(e)=>onChange(e.target.value)}
                className={['control','control--switch',className].filter(a=>a).join(' ')}
                style={style}
                {...props}
            />
            {children}
        </label>
    );
};

export default Control;
