import React from 'react';

const Control = ({
    files, //[{label:'',url:'',base:'',file}]
    multiple = true,
    onChange,
    placeholder,
    style,
    className,
    ...props
}) => {

    return (
       <div>
           File input
           <div>
               { files ? files.map((file,key)=>{
                   return (
                        <h3>{file.label}</h3>
                   );
               }) : null }
           </div>
           <div className="area">
               <input type="file" />
           </div>
        </div>
    );
};

export default Control;
