import React from 'react';

const Control = ({
    value,
    onChange,
    placeholder,
    style,
    className,
    ...props
}) => {
    return (
        <textarea
            placeholder={placeholder}
            value={value}
            onChange={(e)=>onChange(e.target.value)}
            className={['control','control--textarea',className].filter(a=>a).join(' ')}
            style={style}
            {...props}
        />
    );
};

export default Control;
