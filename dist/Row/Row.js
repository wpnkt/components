"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function get() {
    return _Row2.default;
  }
});

var _react = _interopRequireDefault(require("react"));

require("./Row.scss");

var _Row2 = _interopRequireDefault(require("react-bootstrap/Row"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Row = function Row(_ref) {
  var props = Object.assign({}, _ref);
  return _react.default.createElement("div", {
    className: "Row"
  }, _react.default.createElement("div", {
    className: "row"
  }, _react.default.createElement("div", {
    className: "col"
  }, _react.default.createElement("h3", null, "Jaksi cosl"))), _react.default.createElement("h1", null, "component: Row"));
};