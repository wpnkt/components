import React, {Fragment} from 'react';

const Tag = ({
    as,
    ...props
}) => {
    switch(as){
        case 'h1': return <h1 {...props} >{props.children}</h1>;
        case 'h2': return <h2 {...props} >{props.children}</h2>;
        case 'h3': return <h3 {...props} >{props.children}</h3>;
        case 'h4': return <h4 {...props} >{props.children}</h4>;
        case 'h5': return <h5 {...props} >{props.children}</h5>;
        case 'h6': return <h6 {...props} >{props.children}</h6>;
        case 'a': return <a {...props} >{props.children}</a>;
        case 'button': return <button {...props} >{props.children}</button>;
        case 'link': return <a {...props} >{props.children}</a>;
        case 'div': return <div {...props} >{props.children}</div>;
        case 'p': return <p {...props} >{props.children}</p>;
        case 'span': return <span {...props} >{props.children}</span>;
        case 'fragment': return <Fragment {...props} >{props.children}</Fragment>;
        default: return <Fragment {...props} >{props.children}</Fragment>;
    }
};

export default Tag;
