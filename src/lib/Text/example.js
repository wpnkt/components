import React from 'react';
import Text from './Text';
import props from './props';

const options = {
    as: ['span'],
    ...props
};

const examples = Object.entries(options).map(([key,values])=>{
    let options = [];
    values.forEach((value)=>{
        options.push({ [key]: value });
    });
    return { key: value };
});

const components = examples.map(ex=><Text {...ex} />);

export default examples;
