const propsClass = {
    color: [
        'default',
        'primary',
        'gray',
        'light',
        'blue',
        'negative',
    ],
    size: [
        'default',
        'xs',
        'sm',
        'md',
        'lg',
        'xl',
    ],
    transform: [
        'underline',
        'capitalize',
        'uppercase',
        'lowercase',
    ],
    mod: [
        'default',
        'hello',
        'menu',
        'submenu',
        'page-title',
        'card',
        'card--title',
        'accordion',
        'accordion--title',
        'description',
        'label',
        'value',
        'option',
        'editor',
        'table',
        'table--header',
        'copy',
    ]
};

export default  propsClass;
