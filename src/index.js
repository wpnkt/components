import React, {useState} from 'react';
import { render } from "react-dom";
import Text from './lib/Text';
import {
  Control,
  Row,
  Col
} from './lib';

import './lib/dependency';

const Single = ({children}) => {
  return (<Col xs="12" md="6">{children}</Col>);
};
const Section = ({children, title}) => {
  return (<section style={{ margin: '30px 0 30px' }}>
    <Row><Col><h2>{title}</h2></Col></Row>
    <Row>{children}</Row>
  </section>)
};


const App = () => {


  const [valString,setValueString] = useState('');
  const [valBool,setValueBool] = useState(false);
  const [valNumber,setValueNumber] = useState('');
  const [valOpt,setValueOpt] = useState('');

  return (
    <div style={{ width: 640, margin: "15px auto" }}>
      <h1>Hello React</h1>
      <div>
        <Section title="Inputs">
          <Single>
            <Control type="text" onChange={setValueString} value={valString} placeholder="jakis placeholder" />
            </Single>
          <Single>
            <Control type="textarea" onChange={setValueString} value={valString} placeholder="jakis placeholder" />
            </Single>
          <Single>
            <Control type="number" min={1} max={12} onChange={setValueNumber} value={valNumber} placeholder="liczba" />
          </Single>
          <Single>
              <Control
                type="checkbox"
                onChange={setValueBool}
                value={valBool}
                placeholder="liczba"
              >Siemanko kliknij</Control>
          </Single>
          <Single>
            <Control
              type="select"
              onChange={setValueOpt}
              value={valOpt}
              placeholder="liczba"
              options={[{ value: 'xx', label: '-xxx-' },{ value: 'yy', label: '-yyy-' },{ value: 'zz', label: '-zzz-' }]}
            />
            <label>{valOpt}</label>
          </Single>
          <Single>
            <Control
              type="value"
              onChange={setValueOpt}
              value={valOpt}
              placeholder="liczba"
            />
          </Single>

          <Single>
            <Control
              type="switch"
              onChange={setValueOpt}
              value={valOpt}
              placeholder="liczba"
              options={[{ value: 'xx', label: '-xxx-' },{ value: 'yy', label: '-yyy-' },{ value: 'zz', label: '-zzz-' }]}
            >
            Switch option</Control>
          </Single>

          <Single>
            <Control
              type="html"
              onChange={setValueString}
              value={valString}
              placeholder="liczba"
            />
            <label>{valOpt}</label>
          </Single>
          <Single>
          <Control
              type="search"
              onChange={setValueOpt}
              value={valOpt}
              placeholder="liczba"
              options={[{ value: 'xx', label: '-xxx-' },{ value: 'yy', label: '-yyy-' },{ value: 'zz', label: '-zzz-' }]}
            />
            <label>{valOpt}</label>
          </Single>


          <Single>
            <Control
              type="file"
              onChange={setValueOpt}
              value={valOpt}
              placeholder="liczba"
              options={[{ value: 'xx', label: '-xxx-' },{ value: 'yy', label: '-yyy-' },{ value: 'zz', label: '-zzz-' }]}
            />
            <label>{valOpt}</label>
          </Single>


          <Single>
            <Control
              type="select"
              onChange={setValueOpt}
              value={valOpt}
              placeholder="liczba"
              options={[
                { value: 'xx', label: '-xxx-' },
                { value: 'yy', label: '-yyy-' },
                { value: 'zz', label: '-zzz-' }
              ]}
            />
            <label>{valOpt}</label>
          </Single>

        </Section>

        <Text mod={['hello','accordion']} color="blue" size="md xs" className="xxx">dasdasdas</Text>
      </div>
    </div>
  );
}

render(<App />, document.getElementById("root"));
