import React from 'react';

const Control = ({
    value,
    onChange,
    placeholder,
    style,
    className,
    min,
    max,
    ...props
}) => {

    if(min > max){ return false; };
    if(value && value < min){ value = min; };
    if(value && value > max){ value = max; };

    return (
        <input
            min={min}
            max={max}
            type="number"
            placeholder={placeholder}
            value={value}
            onChange={(e)=>onChange(e.target.value)}
            className={['control','control--text',className].filter(a=>a).join(' ')}
            style={style}
            {...props}
        />
    );
};

export default Control;
