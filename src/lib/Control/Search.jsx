import React from 'react';

const Control = ({
    value,
    onChange,
    placeholder,
    style,
    className,
    options,
    ...props
}) => {

    return (
        <select
            type="text"
            placeholder={placeholder}
            value={value}
            onChange={(e)=>onChange(e.target.selectedOptions ? e.target.selectedOptions[0].value : '')}
            className={['control','control--select',className].filter(a=>a).join(' ')}
            style={style}
            {...props}
        >
            { !value ? (
                <option value={''} /*disabled="disabled"*/ >Wybierz</option>
            ) : null }
            { options.map((opt,key)=>{
                return (
                    <option key={key} value={opt.value}>{opt.label}</option>
                );
            }) }
        </select>
    );
};

export default Control;
